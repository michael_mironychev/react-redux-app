/* eslint-disable no-useless-catch */
const { APP_PREFIX } = process.env;

export const setToLocalState = (key, state) => {
  try {
    localStorage.setItem(`${APP_PREFIX}_${key}`, JSON.stringify(state));
  } catch (e) {
    throw e;
  }
};

export const getFromLocalState = (key) => {
  try {
    return JSON.parse(localStorage.getItem(`${APP_PREFIX}_${key}`));
  } catch (e) {
    throw e;
  }
};

export const clearLocalState = (key) => {
  try {
    localStorage.removeItem(`${APP_PREFIX}_${key}`);
  } catch (e) {
    throw e;
  }
};
