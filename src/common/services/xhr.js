/* eslint-disable func-names */
export default function uploadFile({
  url,
  body,
  onError,
  onProgress,
  onComplete,
  headers = [],
  method = 'POST',
}) {
  const xhr = new XMLHttpRequest();

  xhr.upload.onprogress = onProgress;
  xhr.open(method, url, true);

  Object.entries(headers).forEach(([name, value]) => xhr.setRequestHeader(name, value));

  const formData = new FormData();
  const [name, file] = body;

  formData.append(name, file);

  xhr.onreadystatechange = function () {
    if (this.readyState !== 4) {
      return false;
    }

    if (this.status !== 200) {
      return onError(this.statusText);
    }

    const result = JSON.parse(this.response);

    return onComplete(result);
  };

  xhr.send(formData);

  return xhr;
}
