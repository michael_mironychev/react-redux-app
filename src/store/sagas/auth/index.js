import { takeEvery } from 'redux-saga/effects';

import { LOCAL_STORAGE } from '/common/constants/keys';
import { clearLocalState } from '/common/services/ls';
import * as ACTIONS from '/store/types/auth';

function* logoutSaga() {
  yield clearLocalState(LOCAL_STORAGE.ACCESS_TOKEN);
  yield clearLocalState(LOCAL_STORAGE.REFRESH_TOKEN);
}

export default function* authSagas() {
  yield takeEvery(ACTIONS.LOGOUT, logoutSaga);
}
