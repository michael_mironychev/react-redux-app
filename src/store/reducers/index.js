import { combineReducers } from 'redux';

import { ENTITIES } from '/common/constants/redux';

import authState from './auth';

const globalReducer = combineReducers({
  [ENTITIES.AUTH]: authState,
});

export default globalReducer;
