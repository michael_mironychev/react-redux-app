import * as ACTIONS from '/store/types/auth';

const initialState = {
  data: {},
  error: null,
  isProcessing: false,
};

const authReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ACTIONS.GET_AUTH_DATA:
      return {
        ...state,
        error: null,
        isProcessing: true,
      };
    case ACTIONS.FILL_AUTH_DATA:
      return {
        ...state,
        data: payload,
        isProcessing: false,
      };
    case ACTIONS.REQUEST_EXECUTION_ERROR:
      return {
        ...state,
        error: payload,
        isProcessing: false,
      };
    case ACTIONS.CLEAR_AUTH_DATA:
      return initialState;
    default:
      return state;
  }
};

export default authReducer;
