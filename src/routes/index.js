import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { ROUTES } from '/common/constants/routes';
import Login from '/pages/Auth/Login';

const RootRouter = () => (
  <BrowserRouter>
    <Switch>
      <Route path={ROUTES.AUTH}>
        <Login />
      </Route>
    </Switch>
  </BrowserRouter>
);

export default RootRouter;
