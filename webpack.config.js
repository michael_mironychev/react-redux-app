const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { HotModuleReplacementPlugin, NoEmitOnErrorsPlugin, DefinePlugin } = require('webpack');
const path = require('path');

// ENV
const { NODE_ENV = 'development' } = process.env || {};
const DEVELOP_API_URL = 'http://localhost:3010';
const IS_PROD = NODE_ENV === 'production';

const API_URL = IS_PROD ? process.env.API_URL : DEVELOP_API_URL;

// Plugins
const minimizer = [];

if (IS_PROD) minimizer.push(new UglifyJsPlugin());

const devFlagPlugin = new DefinePlugin({
  'process.env.API_URL': JSON.stringify(API_URL),
  'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
});

// Loaders
const fileLoader = { loader: 'file-loader', options: { name: 'assets/[name].[ext]' } };
const postcssLoader = { loader: 'postcss-loader', options: { sourceMap: !IS_PROD } };
const sassLoader = { loader: 'sass-loader', options: { sourceMap: !IS_PROD } };
const styleLoader = !IS_PROD ? 'style-loader' : MiniCssExtractPlugin.loader;
const resourcesLoader = {
  loader: 'sass-resources-loader',
  options: {
    sourceMap: !IS_PROD,
    resources: [
      'src/assets/styles/variables/index.scss',
      'src/assets/styles/keyframes/index.scss',
      'src/assets/styles/mixins/index.scss',
      'src/assets/styles/default/index.scss',
    ],
  },
};
const cssModulesLoader = {
  loader: 'css-loader',
  options: {
    modules: {
      localIdentName: '[name]_[local]_[hash:base64:5]',
    },
    sourceMap: !IS_PROD,
  },
};

// Configuration
module.exports = {
  entry: './src/index.js',
  output: {
    publicPath: '/',
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].[hash].bundle.js',
    chunkFilename: '[name].[hash].bundle.js',
  },
  mode: NODE_ENV,
  devtool: IS_PROD ? '' : 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.(js)x?$/,
        exclude: /node_modules/,
        use: { loader: 'babel-loader' },
      },
      {
        test: /\.(s[ac]|c)ss$/,
        exclude: /\.module\.s[ac]ss$/,
        loader: [styleLoader, 'css-loader', sassLoader, resourcesLoader, postcssLoader],
      },
      {
        test: /\.module\.s[ac]ss$/,
        loader: [styleLoader, cssModulesLoader, sassLoader, resourcesLoader, postcssLoader],
      },
      {
        test: /\.(gif|png|jp(e)?g|eot|ttf|woff(2)?)$/i,
        use: [fileLoader],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack', fileLoader],
      },
    ],
  },
  resolve: {
    alias: {
      '/': path.resolve(__dirname, 'src/'),
      '@/svg': path.resolve(__dirname, 'src/assets/svg'),
      '@/images': path.resolve(__dirname, 'src/assets/images'),
      '@/helpers': path.resolve(__dirname, 'src/common/helpers'),
      '@/services': path.resolve(__dirname, 'src/common/services'),
      '@/constants': path.resolve(__dirname, 'src/common/constants'),
      '@/types': path.resolve(__dirname, 'src/store/types'),
      '@/sagas': path.resolve(__dirname, 'src/store/sagas'),
      '@/actions': path.resolve(__dirname, 'src/store/actions'),
      '@/reducers': path.resolve(__dirname, 'src/store/reducers'),
      '@/selectors': path.resolve(__dirname, 'src/store/selectors'),
    },
  },
  devServer: {
    port: 3000,
    historyApiFallback: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
    },
  },
  plugins: [
    new CopyPlugin({ patterns: [{ from: 'src/assets/**/*', to: 'public/assets/[name].[ext]' }] }),
    new MiniCssExtractPlugin({ filename: 'css/[name].css', chunkFilename: '[id].[hash].css' }),
    new HtmlWebpackPlugin({ template: './src/index.html' }),
    new HotModuleReplacementPlugin(),
    new NoEmitOnErrorsPlugin(),
    new CleanWebpackPlugin(),
    new ProgressBarPlugin(),
    devFlagPlugin,
  ],
  optimization: { minimizer },
};
